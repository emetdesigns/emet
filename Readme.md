# emet

v1.7.0

### A simple, self-hosted, PWA budgeting application.

- [Budgeting Features](#budgeting-features)
- [Application Features](#application-features)
- [Installation](#installation)
- [Usage](#usage)
    - [Transactions](#transactions)
    - [Accounts](#accounts)
    - [Debts](#debts)
    - [Categories](#categories)
    - [Bills](#bills)
    - [Budgets](#budgets)
    - [Settings](#settings)
- [License](#license)
- [Contributing](#contributing)
- [Credits](#credits)

---

## Budgeting Features

- Simple account-to-account based transaction system
- Track total expenses, revenue, and net income
- Set up to 2 personal accounts to instantly know balances
- Track bills, debts, and budgets
- Search through transactions, accounts, and categories
- Filter financial data by month, year, and all-time
- Sort financial data by name, date, account, amount, or category
- Multiple locale/currency support using javascript's built-in Intl.NumberFormat
- Set transactions as `Reconciled` to keep track with your bank statements.

---

## Application Features

- No libraries or frameworks, just pure HTML, PHP, Javascript, and CSS
- Easy Installation, just upload onto your own web server
- PWA Installable, with offline viewing of financial data
- Single user authentication
- Responsive design for desktop, tablet, and mobile

---

## Installation

- Download/clone the source code and extract into your web directory's root folder
- Default username/password is admin/admin

---

## Usage

There are three main types of financial data: [Transactions](#transactions), [Accounts](#accounts) and [Categories](#categories), all other types of data derive from these. [Debts](#debts) are a special type of account, and [Bills](#bills) and [Budgets](#budgets) are a special type of category.

`Accounts` and `Categories` are created dynamically as you create `Transactions`. In order to start tracking an account/category, you need to first create a transaction with that account/category. Everything is `string based` therefore simply typing in a new account/category when creating a transaction will create that account/category.

> **NOTE:** Account/category names are case sensitive, so creating a transaction with category `Groceries` and another with category `groceries` will create two different categories.

### Transactions

Transactions are account-to-account based, meaning that every transaction has a `from` account and a `to` account. To create a new transaction simply click on the `+` in the lower right corner, and fill in the information for `date`, `from`, `to`, `amount`, `category` and check/uncheck `reconciled`. Accounts/categories will automatically be created based on the information you provide.

To edit a transaction, simply select it, and a form will open up to edit or delete the transaction.

### Accounts

`Accounts` are created based on the transactions you have created. In order to start seeing your accounts, you should first create transactions with those accounts. For example to start off a new Checking account, you could create a new transaction with:

> `From`: Balance  
> `To`: Checking  
> `Amount`: $500.00

which would create a new `Checking` account which you can then set as your primary account in [Settings](#settings). `Balance` would be a dummy account your initial balance is coming from.

### Debts

`Debts` are a special type of `Account`. By clicking on an account you can change it's settings which include `Bill date`, `Budget amount` and `Limit`. By setting a date, amount and limit on an account you can start tracking it as a `Debt`. The `Bill date` value is the day of the month (1-31) your debt payment is due, the `Budget amount` is how much your monthly debt payment is, and the `Limit` is the total debt limit, this can be your total loan amount or your credit card limit. Any value for the due date greater than `0` will begin to track the account as a debt. In order to stop tracking a debt, simply set the `Bill date` back to `0`.

> **NOTE:** `Budget amount` and `Limit` do not have to be greater than `0` in order for the debt to be tracked, but if you don't have a monthly payment or a limit, why are you tracking it? :P

> **NOTE:** In order to properly have debts with a negative account balance (money you owe) you should create a transaction with the money going `from` your debt account `to` another account. For example, you could create a new debt to start tracking by creating a transaction with:
>
> `From`: Discover Card  
> `To`: Discover  
> `Amount`: $500.00  
> `Category`: Debt
>
> This would create a negative balance for the account `Discover Card` which you can then edit and add a `Bill date`, `Budget amount`, and `Limit` to in order to start tracking as a debt.

### Categories

`Categories` are also created based on the transactions you have created. In order to start seeing your categories, you should first create transactions with those categories. For example to start tracking how much you pay for groceries, you could create a new transaction with:

> `From`: Checking
> `To`: Natural Grocers
> `Amount`: $30.00
> `Category`: Groceries

which would create a new `Groceries` category which you can track if desired.

### Bills

`Bills` are a special type of `Category`. By clicking on a category you can change it's settings which include `Bill date` and `Budget amount`. By setting the `Bill date` and `Budget amount` on a category you can start tracking it as a `Bill`. The `Bill date` value is the day of the month (1-31) your monthly bill is due, the `Budget amount` is how much your (average) monthly payment is. Any value for `Bill date` greater than `0` will begin to track the category as a `Bill`. In order to stop tracking a bill, simply set the `Bill date` back to `0`.

> **NOTE:** `Budget amount` does not have to be greater than `0` in order for the bill to be tracked, but if you're tracking a monthly bill I'm assuming you're making monthly payments?

### Budgets

`Budgets` are a special type of `Category`. By clicking on a category you can change it's settings which include `Bill date` and `Budget amount`. A `Budget` is a tracked category without a `Bill date`. As opposed to a `Bill` the `Bill date` remains `0` and the `Budget amount` turns into the maximum amount you want to spend in this category. In order to stop tracking a budget, simply set the `Budget amount` back to `0`.

> **NOTE:** Setting the `Budget amount` back to `0` but making `Bill date` greater than `0` will make the category a `Bill`.

### Settings

Some settings can be configured by clicking on the gear icon in the top right.

- `Username` will change the username you use to login
- Submitting the settings form with any value for the `Password` field will change the password you use to login
- `Primary Account` is for setting the name of the 1st account you would like to track under the `Personal Accounts` section
- `Secondary Account` is for setting the name of the 2nd account you would like to track under the `Personal Accounts` section
- `Locale` is for setting the locale used for the currency format, for example `en-US` or `fr-CA`. [See this site](https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPInternational/LanguageandLocaleIDs/LanguageandLocaleIDs.html) for more information and what codes are available.
- `Currency` is for setting the currency type for the currency format, for example `USD` or `EUR`. [See this site](https://www.six-group.com/en/products-services/financial-information/data-standards.html#scrollTo=currency-codes) for more information and what codes are available.
- `Minimum Digits` is the accuracy of decimal places. Default is `2` decimal places which would display `$5.00`.
- `Currency Format` is the type of formatting to show, whether `accounting` (default - with parentheses wrapping the amount) or `standard` (with a minus sign before the amount).

> **NOTE:** You can also logout within the settings menu by clicking on the `Logout` button at the top next to the `Settings` text.

> **NOTE:** Passwords are not currently checked for *robustness*. The application's username/password is just a simple security measure to deter people from snooping/tampering with your data. **USE AT YOUR OWN RISK**. If you are not comfortable with exposing your personal financial data to the world then **DON'T STORE IT ONLINE**.

---

## License

Emet is under the **Open Source GPL3 License**, which means you can use, study, change and share it at will. From the full source, anyone can build, fork and use as they wish. Emet does not collect or send any data in any way whatsoever. I am also not responsible for anything that may happen to your personal financial data. **USE AT YOUR OWN RISK**.

---

## Contributing

Feel free to open a pull request or issue as you like if you run into a bug or feel something is missing or should be changed. I will get to things as I have time, but I am not a developer or programmer, just a hobbyist tinkering around with web applications, so please be patient.

---

## Credits

- Icons used were downloaded from [Ionicons](https://ionic.io/ionicons) and then edited slightly for my own personal use. All icons are embedded svg elements within icons.js.
- The font used is [Ubuntu](https://design.ubuntu.com/font/) and was downloaded using the [Google Webfonts Helper](https://github.com/majodev/google-webfonts-helper)