<?php

session_start();

$bypass = false;

if(isset($_POST['username']) && !isset($_SESSION['username'])) {
	require 'data.php';
	$settings = unserialize($raw_settings);
	if(($settings['username'] == $_POST['username'] && password_verify($_POST['password'], $settings['password'])) || $bypass) {
		if(isset($_POST['remember'])) {
			$days = 30;
			$val = encryptCookie($settings['username'], $settings['randkey']);
			setcookie('remember', $val, time() + ($days * 24 * 60 * 60 * 1000));
		}
		$_SESSION['username'] = $_POST['username'];
	}
	if(!isset($_SESSION['username'])) $failed = true;
}

if(isset($_SESSION['username'])) {
	header('Location: /');
	exit();
} else if(isset($_COOKIE['remember'])) {
	require 'data.php';
	$settings = unserialize($raw_settings);
	$username = decryptCookie($_COOKIE['remember'], $settings['randkey']);
	
	if($username == $settings['username']) {
		$_SESSION['username'] = $username;
		header('Location: /');
		exit();
	}
}

function encryptCookie($data, $key) {
	$encryption_key = base64_decode($key);
	$cipher = 'aes-256-cbc';
	$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher));
	$encrypted = openssl_encrypt($data, $cipher, $encryption_key, 0, $iv);
	
	return base64_encode($encrypted . '::' . $iv);
}
 
function decryptCookie($data, $key) {
	$encryption_key = base64_decode($key);
	$cipher = 'aes-256-cbc';
	list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
	
	return openssl_decrypt($encrypted_data, $cipher, $encryption_key, 0, $iv);
}

?>