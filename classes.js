// CLASSES JS

months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

class Setting {
	constructor(obj) {
		this.type = 'setting';
		this.id = 1;
		this.username = obj.username;
		this.password = obj.password;
		this.randkey = obj.randkey;
		this.primary = obj.primary;
		this.secondary = obj.secondary;
		this.locale = obj.locale;
		this.currency = obj.currency;
		this.digits = obj.digits;
		this.format = obj.format;
		this.expenses = { all: 0 };
		this.expense = 0;
		this.revenues = { all: 0 };
		this.revenue = 0;
		this.nets = { all: 0 };
		this.net = 0;
		
		this.categories = obj.categories;
		this.accounts = obj.accounts;
	}
	
	checked(val) {
		return val ? 'checked' : '';
	}
	
	setPeriod(prd) {
		this.period = prd;
	}
	
	setCurrent(cur) {
		this.current = cur;
	}
	
	sign(amt) {
		if(amt) return amt < 0 ? 'neg' : 'pos';
		else return '';
	}
	
	money(amt) {
		if(amt) {
			return new Intl.NumberFormat(this.locale, {
				style:					'currency',
				currency:				this.currency,
				minimumFractionDigits:	this.digits,
				currencySign:			this.format
			}).format(amt / 100);
		} else return '-';
	}
	
	transfer(trans) {
		let mKey = trans.date.split('-')[0] + '-' + parseInt(trans.date.split('-')[1]);
		let yKey = trans.date.split('-')[0] + '';
		
		let state = 'state';
		
		if(trans.from == this.primary) {
			if(trans.to != this.secondary) state = 'expense'; // Expense
		}
		else if(trans.from == this.secondary) {
			if(trans.to != this.primary) state = 'expense'; // Expense
		}
		else if(Object.keys(this.accounts).includes(trans.from)) {
			if(trans.to != this.secondary && trans.to != this.primary && !Object.keys(this.accounts).includes(trans.to)) {
				state = 'expense'; // Expense
			}
		}
		else if(trans.to == this.primary || trans.to == this.secondary) state = 'revenue'; // Revenue
		// ELSE Transfer
		
		switch(state) {
			case 'expense':
				if(trans.category.toUpperCase() == 'BALANCE') break;
				this.expenses.all -= trans.amount;
				this.nets.all -= trans.amount;
				
				if(this.expenses[mKey]) this.expenses[mKey] -= trans.amount;
				else this.expenses[mKey] = trans.amount * -1;
				if(this.expenses[yKey]) this.expenses[yKey] -= trans.amount;
				else this.expenses[yKey] = trans.amount * -1;
				
				if(this.nets[mKey]) this.nets[mKey] -= trans.amount;
				else this.nets[mKey] = trans.amount * -1;
				if(this.nets[yKey]) this.nets[yKey] -= trans.amount;
				else this.nets[yKey] = trans.amount * -1;
				
				break;
			case 'revenue':
				if(trans.category.toUpperCase() == 'BALANCE') break;
				this.revenues.all += trans.amount;
				this.nets.all += trans.amount;
				
				if(this.revenues[mKey]) this.revenues[mKey] += trans.amount;
				else this.revenues[mKey] = trans.amount;
				if(this.revenues[yKey]) this.revenues[yKey] += trans.amount;
				else this.revenues[yKey] = trans.amount;
				
				if(this.nets[mKey]) this.nets[mKey] += trans.amount;
				else this.nets[mKey] = trans.amount;
				if(this.nets[yKey]) this.nets[yKey] += trans.amount;
				else this.nets[yKey] = trans.amount;
				
				break;
		}
		
	}
	
	printExpenses(key) {
		if(this.expenses[key]) this.expense = this.expenses[key];
		else this.expense = 0;
		return `
			<span class="name">Total Expenses</span>
			<span class="balance ${this.sign(this.expense)}">${this.money(this.expense)}</span>
		`;
	}
	
	printRevenue(key) {
		if(this.revenues[key]) this.revenue = this.revenues[key];
		else this.revenue = 0;
		return `
			<span class="name">Total Revenue</span>
			<span class="balance ${this.sign(this.revenue)}">${this.money(this.revenue)}</span>
		`;
	}
	
	printNet(key) {
		if(this.nets[key]) this.net = this.nets[key];
		else this.net = 0;
		return `
			<span class="name">Net Income</span>
			<span class="balance ${this.sign(this.net)}">${this.money(this.net)}</span>
		`;
	}
	
	printAccounts(amt) {
		return `
			<span class="name">Total</span>
			<span class="balance ${this.sign(amt)}">${this.money(amt)}</span>
		`;
	}
	
	form() {
		return `
			<label class="text">Username
				<input type="text" name="username" value="${this.username}" required>
			</label>
			<label class="text">Password
				<input type="password" name="password" value="">
			</label>
			<label class="text">Key
				<input type="text" name="randkey" value="${this.randkey}" required>
			</label>
			<hr>
			<label class="list text" data-list="account">Primary Account
				<input type="text" name="primary" value="${this.primary}" required>
				<ul class="datalist hidden" tabindex="-1"></ul>
			</label>
			<label class="list text" data-list="account">Secondary Account
				<input type="text" name="secondary" value="${this.secondary}" required>
				<ul class="datalist hidden" tabindex="-1"></ul>
			</label>
			<hr>
			<label class="text">Locale
				<input type="text" name="locale" value="${this.locale}" required>
			</label>
			<label class="text">Currency
				<input type="text" name="currency" value="${this.currency}" required>
			</label>
			<label class="text">Minimum Digits
				<input type="number" name="digits" value="${this.digits}" required min="0" max="3" step="1">
			</label>
			<label class="text">Currency Format
				<input type="text" name="format" value="${this.format}" required>
			</label>
			<a id="export" class="text" href="/export.php">Export Transactions to CSV</a>
			<a id="import" class="text">Import Transactions from CSV</a>
			<label id="import-label" class="alert hidden">
				WARNING: THIS WILL OVERRIDE ALL DATA
				<input type="file" name="import" accept=".csv">
			</label>
		`;
	}
}

class Finance {
	constructor(settings) {
		this.locale = settings.locale;
		this.currency = settings.currency;
		this.digits = settings.digits;
		this.format = settings.format;
		this.primary = settings.primary;
		this.secondary = settings.secondary;
		
		this.type = 'finance';
		this.balances = { all: 0 };
		this.balance = 0;
		
		this.count = 0;
		
		this.accounts = settings.accounts;
	}
	
	money(amt) {
		if(amt) {
			return new Intl.NumberFormat(this.locale, {
				style:					'currency',
				currency:				this.currency,
				minimumFractionDigits:	this.digits,
				currencySign:			this.format
			}).format(amt / 100);
		} else return '-';
	}
	
	sign(amt) {
		if(amt) return amt < 0 ? 'neg' : 'pos';
		else return '';
	}
	
	floating(amt) {
		return amt / 100;
	}
	
	floatStr(amt) {
		return amt != 0 ? amt / 100 : '';
	}
	
	paid(amt) {
		if(amt) return 'paid';
		else return '';
	}
	
	setType(typ) {
		this.type = typ;
	}
	
	transfer(trans, factor) {
		let amt = trans.amount * factor;
		this.balances.all += amt;
		this.count++;
		
		let mKey = trans.date.split('-')[0] + '-' + parseInt(trans.date.split('-')[1]);
		let yKey = trans.date.split('-')[0] + '';
		
		if(this.balances[mKey]) this.balances[mKey] += amt;
		else this.balances[mKey] = amt;
		
		if(this.balances[yKey]) this.balances[yKey] += amt;
		else this.balances[yKey] = amt;
	}
	
	print(key) {
		if(this.balances[key]) {
			this.balance = this.balances[key];
			return `
				<span class="name">${this.name}</span>
				<span class="balance ${this.sign(this.balances[key])}">${this.money(this.balances[key])}</span>
			`;
		} else return '';
	}
}

class Account extends Finance {
	constructor(settings, name = '') {
		super(settings);
		
		this.type = 'account';
		this.name = name;
		
		if(this.accounts[name]) {
			this.day = this.accounts[name].day;
			this.budget = this.accounts[name].budget;
			this.limit = this.accounts[name].limit;
			this.remaining = 0;
		} else {
			this.day = 0;
			this.budget = 0;
			this.limit = 0;
			this.remaining = 0;
		}
	}
	
	transfer(trans, factor) {
		let amt = trans.amount * factor;
		this.balances.all += amt;
		this.count++;
		
		let mKey = trans.date.split('-')[0] + '-' + parseInt(trans.date.split('-')[1]);
		let yKey = trans.date.split('-')[0] + '';
		
		if(this.balances[mKey]) this.balances[mKey] += amt;
		else this.balances[mKey] = amt;
		
		if(this.balances[yKey]) this.balances[yKey] += amt;
		else this.balances[yKey] = amt;
		
		this.remaining = Math.abs(this.balances.all);
	}
	
	getText() {
		return this.name.toLowerCase();
	}
	
	money(amt) {
		if(amt) return new Intl.NumberFormat(this.locale, {
			style:					'currency',
			currency:				this.currency,
			minimumFractionDigits:	this.digits,
			currencySign:			this.format
		}).format(amt / 100);
		else return '-';
	}
	
	width(amt) {
		return Math.min(100, (this.floating(amt) / this.floating(this.limit)) * 100);
	}
	
	blank() {
		return `
			<span class="name">${this.name}</span>
			<span class="balance light">NO DATA</span>
		`;
	}
	
	isDue(key) {
		if(!this.balances[key] && this.day > 0) return true;
		else return false;
	}
	
	form() {
		console.log('>> OPENING FORM', this.name);
		return `
			<label>
				<span class="icon" data-icon="account"></span><input type="text" name="newname" value="${this.name}" required>
			</label>
			<label>
				<span class="icon" data-icon="bill"></span><input type="number" name="day" value="${this.day}" required min="0" step="1" max="31">
			</label>
			<label>
				<span class="icon" data-icon="budget"></span><input type="number" name="budget" value="${this.floating(this.budget)}" required min="0" step="0.01">
			</label>
			<label>
				<span class="icon" data-icon="limit"></span><input type="number" name="limit" value="${this.floating(this.limit)}" required min="0" step="0.01">
			</label>
		`;
	}
	
	printDebt(key) {
		if(this.balances[key]) this.balance = this.balances[key];
		else this.balance = 0;
		
		return `
			<span class="name ${this.paid(this.balance)}">${this.name}</span>
			<span class="balance ${this.sign(this.balance)} ${this.paid(this.balance)}">${this.money(this.balance)}</span>
			<span class="day ${this.paid(this.balance)}">${this.day}</span>
			<span class="budget ${this.paid(this.balance)}">${this.money(this.budget)}</span>
			<span class="inner" style="width:${this.width(this.remaining)}%"></span>
			<span class="gauge"></span>
			<span class="remaining">${this.money(this.remaining)}</span>
			<span class="limit">${this.money(this.limit)}</span>
		`;
	}
	
	print(key) {
		if(this.balances[key]) this.balance = this.balances[key];
		else this.balance = 0;
		
		return `
			<span class="name ${this.paid(this.balance)}">${this.name}</span>
			<span class="balance ${this.sign(this.balance)} ${this.paid(this.balance)}">${this.money(this.balance)}</span>
		`;
	}
}

class Category extends Finance {
	constructor(settings, name = '') {
		super(settings);
		
		this.type = 'category';
		this.name = name;
		
		if(settings.categories[name]) {
			this.day = settings.categories[name].day;
			this.budget = settings.categories[name].budget;
		} else {
			this.day = 0;
			this.budget = 0;
		}
	}
	
	money(amt) {
		if(amt) return new Intl.NumberFormat(this.locale, {
			style:					'currency',
			currency:				this.currency,
			minimumFractionDigits:	this.digits,
			currencySign:			this.format
		}).format(amt / 100);
		else return '-';
	}
	
	width(amt) {
		return Math.min(100, (this.floating(amt) / this.floating(this.budget)) * 100);
	}
	
	over(amt) {
		if(amt > this.budget) return 'over';
		else return '';
	}
	
	getText() {
		return this.name.toLowerCase();
	}
	
	getBalance(key) {
		if(this.balances[key]) return this.balances[key];
		else return 0;
	}
	
	isSlice(key) {
		if(this.balances[key] && this.balances[key] < 0 && this.name.toUpperCase() != 'BALANCE') return true;
		else return false;
	}
	
	isDue(key) {
		if(!this.balances[key] && this.day > 0) return true;
		else return false;
	}
	
	form() {
		return `
			<label>
				<span class="icon" data-icon="category"></span><input type="text" name="newname" value="${this.name}" required>
			</label>
			<label>
				<span class="icon" data-icon="bill"></span><input type="number" name="day" value="${this.day}" required min="0" step="1" max="31">
			</label>
			<label>
				<span class="icon" data-icon="budget"></span><input type="number" name="budget" value="${this.floating(this.budget)}" required min="0" step="0.01">
			</label>
		`;
	}
	
	printBill(key) {
		if(this.balances[key]) this.balance = Math.abs(this.balances[key]);
		else this.balance = 0;
		
		return `
			<span class="name ${this.paid(this.balance)}">${this.name}</span>
			<span class="day ${this.paid(this.balances[key])}">${this.day}</span>
			<span class="budget ${this.paid(this.balance)}">${this.money(this.budget)}</span>
			<span class="balance ${this.over(this.balance)} ${this.sign(this.balance)} ${this.paid(this.balance)}">${this.money(this.balance)}</span>
		`;
	}
	
	printBudget(key) {
		if(this.balances[key]) this.balance = Math.abs(this.balances[key]);
		else this.balance = 0;
		
		return `
			<span class="name ${this.paid(this.balance)}">${this.name}</span>
			<span class="inner ${this.over(this.balance)}" style="width:${this.width(this.balance)}%"></span>
			<span class="gauge"></span>
			<span class="budget">${this.money(this.budget)}</span>
			<span class="balance ${this.over(this.balance)} ${this.sign(this.balance)} ${this.paid(this.balance)}">${this.money(this.balance)}</span>
		`;
	}
	
	print(key) {
		if(this.balances[key]) this.balance = this.balances[key];
		else this.balance = 0;
		
		return `
			<span class="name ${this.paid(this.balance)}">${this.name}</span>
			<span class="balance ${this.over(this.balance)} ${this.sign(this.balance)} ${this.paid(this.balance)}">${this.money(this.balance)}</span>
		`;
	}
}

class Transaction extends Finance {
	constructor(settings, obj = null) {
		super(settings);
		
		this.type = 'transaction';
		if(obj) {
			this.id = obj.id;
			this.date = obj.date;
			this.from = obj.from;
			this.to = obj.to;
			this.amount = obj.amount;
			this.category = obj.category;
			this.reconciled = obj.reconciled;
			
			this.flow = this.setFlow();
		} else {
			let today = new Date();
			const offset = today.getTimezoneOffset();
			
			this.id = 0;
			this.date = new Date(today.getTime() - (offset * 60 * 1000)).toISOString().split('T')[0];
			this.from = '';
			this.to = '';
			this.amount = 0;
			this.category = '';
			this.reconciled = 0;
			
			this.flow = '';
		}
	}
	
	day() {
		let dYear = parseInt(this.date.split('-')[0]);
		let dMonth = parseInt(this.date.split('-')[1]);
		let dDate = parseInt(this.date.split('-')[2]);
		
		return months[dMonth - 1] + ' ' + dDate + ', ' + dYear;
	}
	
	getText() {
		return this.date + ' ' + this.from.toLowerCase() + ' ' +
			this.to.toLowerCase() + ' ' + this.money(this.amount) + ' ' +
			this.category.toLowerCase() + ' ' + this.floatStr(this.amount);
	}
	
	setFlow() {
		if(this.from == this.primary) {
			if(this.to != this.secondary) return 'expense'; // EXPENSE
			else return 'transfer'; // TRANSFER
		}
		else if(this.from == this.secondary) {
			if(this.to != this.primary) return 'expense'; // EXPENSE
			else return 'transfer'; // TRANSFER
		}
		else if(Object.keys(this.accounts).includes(this.from)) {
			if(this.to != this.secondary && this.to != this.primary && !Object.keys(this.accounts).includes(this.to)) {
				return 'expense'; // EXPENSE
			} else return 'transfer';
		}
		else if(this.to == this.primary || this.to == this.secondary) return 'revenue'; // REVENUE
		else return 'transfer'; // TRANSFER
	}
	
	getFlow() {
		switch(this.flow) {
			case 'expense': return 'neg';
			case 'revenue': return 'pos';
			case 'transfer': return 'light';
			default: return '';
		}
	}
	
	getReconciled() {
		return this.reconciled > 0 ? `<span class="icon" data-icon="reconciled"></span>` : '';
	}
	
	getChecked(val) {
		return val ? 'checked' : '';
	}
	
	print(key) {
		let dStr = this.date.split('-')[0] + '-' + parseInt(this.date.split('-')[1]);
		let amt = this.amount;
		if(dStr.includes(key) || key == 'all') {
			if(this.flow == 'expense') amt = Math.abs(amt) * -1;
			return `
				<span class="date">${this.day()}</span>
				<span class="from">${this.from}</span>
				<span class="to">${this.to}</span>
				<span class="amount ${this.getFlow()}">${this.money(amt)}</span>
				<span class="category">${this.category}</span>
				<span class="reconciled">${this.getReconciled()}</span>
			`;
		} else return '';
	}
	
	form() {
		return `
			<label>
				<span class="icon" data-icon="date"></span><input type="date" name="date" value="${this.date}" required>
			</label>
			<label class="list" data-list="account">
				<span class="icon" data-icon="from"></span><input type="text" name="from" value="${this.from}" required>
				<ul class="datalist hidden" tabindex="-1"></ul>
			</label>
			<label class="list" data-list="account">
				<span class="icon" data-icon="to"></span><input type="text" name="to" value="${this.to}" required>
				<ul class="datalist hidden" tabindex="-1"></ul>
			</label>
			<label>
				<span class="icon" data-icon="amount"></span><input type="number" name="amount" value="${this.floatStr(this.amount)}" required min="0.01" step="0.01">
			</label>
			<label class="list" data-list="category">
				<span class="icon" data-icon="category"></span><input type="text" name="category" value="${this.category}" required>
				<ul class="datalist hidden" tabindex="-1"></ul>
			</label>
			<label class="checkbox" for="reconciled">
				<span class="icon" data-icon="reconciled"></span>
				<input id="reconciled" type="checkbox" name="reconciled" ${this.getChecked(this.reconciled)}>
				<div class="box"><span class="slider"></span></div>
			</label>
		`;
	}
}
