<?php
// Export to CSV

require 'protect.php';
require 'data.php';
$transactions = unserialize($raw_transactions);

$day = new DateTime('today');

$filename = 'emet-export_' . $day->format('Y-m-d') . '.csv';
$delimeter = ',';

$f = fopen('php://memory', 'w');

$headings = array('date', 'from', 'to', 'amount', 'category', 'reconciled');

fputcsv($f, $headings, $delimeter);

foreach($transactions as $transaction) {
	$amt = (float) $transaction['amount'] / 100;
	$row = array(
		$transaction['date'],
		$transaction['from'],
		$transaction['to'],
		$amt,
		$transaction['category'],
		$transaction['reconciled']
	);
	
	fputcsv($f, $row, $delimeter);
}

fseek($f, 0);

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="' . $filename . '";');

fpassthru($f);
fclose($f);

exit();

?>