<?php require 'protect.php'; ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Emet: a simple, self-hosted, PWA budgeting application - Main">
	<link rel="stylesheet" href="styles.css">
	
	<link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
	<link rel="manifest" href="/icons/site.webmanifest">
	<link rel="mask-icon" href="/icons/safari-pinned-tab.svg" color="#21bda0">
	<link rel="shortcut icon" href="/icons/favicon.ico">
	<meta name="msapplication-TileColor" content="#21bda0">
	<meta name="msapplication-config" content="/icons/browserconfig.xml">
	<meta name="theme-color" content="#21bda0">
	
	<title>emet</title>
</head>

<body>
	<header>
		<button id="logo-button" class="icon link" type="button" data-icon="logo" data-link="home"></button>
		<input id="search" type="text" name="search" placeholder="Search...">
		<button id="search-close" class="icon hidden" type="button" data-icon="close"></button>
		<div id="spacer"></div>
		<div class="menu filter">
			<button class="icon opener" type="button" data-icon="calendar"></button>
			<div class="inner hidden">
				<h3>Filter By</h3>
				<button class="filter" type="button" data-filter="all">All Time<span class="icon" data-icon="submit"></span></button>
				<button class="filter" type="button" data-filter="yearly">Yearly<span class="icon" data-icon="submit"></span></button>
				<button class="filter" type="button" data-filter="monthly">Monthly<span class="icon" data-icon="submit"></span></button>
				<div class="selector">
					<button class="icon prev" type="button" data-icon="previous"></button>
					<button class="current" type="button"></button>
					<button class="icon next" type="button" data-icon="next"></button>
				</div>
			</div>
		</div>
		<button id="settings-button" class="icon" type="button" data-icon="settings"></button>
	</header>
	
	<nav>
		<button id="home-button" class="icon link selected" type="button" data-icon="home" data-text="Home" data-link="home"></button>
		<button id="transaction-button" class="icon link" type="button" data-icon="transaction" data-text="Transactions" data-link="transaction"></button>
		<button id="account-button" class="icon link" type="button" data-icon="account" data-text="Accounts" data-link="account"></button>
		<button id="category-button" class="icon link" type="button" data-icon="category" data-text="Categories" data-link="category"></button>
		<button id="data-button" class="icon link" type="button" data-icon="data" data-text="Data" data-link="data"></button>
	</nav>
	
	<main>
		<div class="col1">
			<section id="recent">
				<h2>RECENT TRANSACTIONS<span data-key="key">recent</span></h2>
				<ul></ul>
				<button id="more-button" class="link hidden" type="button" data-link="transaction">Show More</button>
			</section>
			<section id="transactions" class="hidden">
				<div class="header">
					<h2>TRANSACTIONS<span data-key="key">transactions</span></h2>
					<div class="menu sort" data-sort="date,from,to,amount,category"></div>
				</div>
				<ul></ul>
			</section>
			<section id="accounts" class="hidden">
				<div class="header">
					<h2>ACCOUNTS<span data-key="key">accounts</span></h2>
					<div class="menu sort" data-sort="name,balance"></div>
				</div>
				<ul></ul>
			</section>
			<section id="categories" class="hidden">
				<div class="header">
					<h2>CATEGORIES<span data-key="key">categories</span></h2>
					<div class="menu sort" data-sort="name,balance"></div>
				</div>
				<ul></ul>
			</section>
			<section id="top-categories" class="hidden">
				<h2>TOP CATEGORIES<span data-key="key">top</span></h2>
				<ul></ul>
			</section>
		</div>
		
		<div class="col2">
			<section id="personal">
				<h2>PERSONAL ACCOUNTS<span data-key="key">personal</span></h2>
				<h3 id="primary-account"></h3>
				<h3 id="secondary-account"></h3>
				<hr>
				<h3 id="total-account"></h3>
			</section>
			<section id="flow">
				<h2>CASH FLOW<span data-key="key">flow</span></h2>
				<h3 id="total-expenses"></h3>
				<h3 id="total-revenue"></h3>
				<hr>
				<h3 id="total-net"></h3>
			</section>
			<section id="due" class="full">
				<h2>DUE<span data-key="month">due</span></h2>
				<ul></ul>
			</section>
			<section id="debts" class="full hidden">
				<h2>DEBTS<span data-key="month">debts</span></h2>
				<ul></ul>
			</section>
			<section id="bills" class="full hidden">
				<h2>BILLS<span data-key="month">bills</span></h2>
				<ul></ul>
			</section>
			<section id="budgets" class="full">
				<h2>BUDGETS<span data-key="month">budgets</span></h2>
				<ul></ul>
			</section>
			<section id="top-categories-pie" class="hidden">
				<h2>TOP CATEGORIES<span data-key="key">top</span></h2>
				<div class="pie"></div>
			</section>
		</div>
	</main>
	
	<button id="add-button" class="icon" type="button" data-icon="add"></button>
	
	<span id="offline" class="hidden">OFFLINE</span>
	
	<div id="overlay" class="overlay hidden"></div>
	<div id="loading" class="overlay"><span>LOADING</span><span class="icon" data-icon="loading"></span></div>
	
	<form id="form" class="hidden" autocomplete="off">
		<h2 id="form-title">Form</h2>
		<input id="form-type" type="hidden" name="type" value="type" required disabled>
		<input id="form-id" type="hidden" name="id" value="0" required disabled>
		<div id="form-inputs"></div>
		<div id="form-buttons">
			<button id="form-submit" class="icon" type="submit" name="submit" data-icon="submit"></button>
			<button id="form-edit" class="icon" type="button" name="edit" data-icon="edit">Edit</button>
			<button id="form-delete" class="icon" type="button" name="delete" data-icon="delete"></button>
			<button id="form-cancel" class="icon" type="button" name="cancel" data-icon="cancel"></button>
		</div>
		<button id="form-close" class="icon" type="button" name="close" data-icon="close"></button>
	</form>
	<form id="logout-form" class="hidden" method="post">
		<input type="hidden" name="logout" value="1">
		<button class="icon" data-icon="logout" type="submit"></button>
	</form>
	
	<script>window.onload = () => { 'use strict'; if('serviceWorker' in navigator) navigator.serviceWorker.register('./sw.js'); }</script>
	<script src="icons.js"></script>
	<script src="classes.js"></script>
	<script src="main.js"></script>
</body>

</html>