<?php require 'check.php'; ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Emet: a simple, self-hosted, PWA budgeting application - Login">
	<link rel="stylesheet" href="styles.css">
	
	<link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
	<link rel="manifest" href="/icons/site.webmanifest">
	<link rel="mask-icon" href="/icons/safari-pinned-tab.svg" color="#21bda0">
	<link rel="shortcut icon" href="/icons/favicon.ico">
	<meta name="msapplication-TileColor" content="#21bda0">
	<meta name="msapplication-config" content="/icons/browserconfig.xml">
	<meta name="theme-color" content="#21bda0">
	
	<title>emet</title>
</head>

<body class="flex">
	<span id="logo" class="icon" data-icon="logo-banner"></span>
	<form id="login-form" method="post" target="_self">
		<h2>Sign In</h2>
		<div id="form-inputs">
			<label class="text" for="username">Username
				<input id="username" type="text" name="username" required>
			</label>
			<label class="text" for="password">Password
				<input id="password" type="password" name="password" required>
			</label>
			<label class="text checkbox" for="remember">Remember Me
				<input id="remember" type="checkbox" name="remember">
				<div class="box"><span class="slider"></span></div>
			</label>
		</div>
		<div id="form-buttons">
			<button id="form-login" class="text" type="submit" name="submit">Login</button>
		</div>
	</form>
	<?php if(isset($failed)) { ?> <p id="login-bad" class="alert">Invalid username or password</p> <?php } ?>
	<?php if($bypass) { ?> <p id="default" class="alert"><span class="neg">BYPASS = TRUE</span><br>Any login will work</p> <?php } ?>
	
	<span id="offline" class="hidden">OFFLINE</span>
	
	<script src="icons.js"></script>
	<script>
	document.addEventListener('DOMContentLoaded', (event) => {
		printIcons();
	});
	</script>
</body>
</html>