// MAIN JS

document.addEventListener('DOMContentLoaded', (event) => {
	
	printMenus();
	printIcons();
	
	const recentList = document.querySelector('#recent ul');
	const transactionList = document.querySelector("#transactions ul");
	const accountList = document.querySelector('#accounts ul');
	const categoryList = document.querySelector('#categories ul');
	const billList = document.querySelector('#bills ul');
	const budgetList = document.querySelector('#budgets ul');
	const debtList = document.querySelector('#debts ul');
	const dueList = document.querySelector('#due ul');
	const topCategoriesPie = document.querySelector('#top-categories-pie .pie');
	const topCategoriesList = document.querySelector('#top-categories ul');
	const primaryAccount = document.querySelector('#primary-account');
	const secondaryAccount = document.querySelector('#secondary-account');
	const totalAccount = document.querySelector('#total-account');
	const totalExpenses = document.querySelector('#total-expenses');
	const totalRevenue = document.querySelector('#total-revenue');
	const totalNet = document.querySelector('#total-net');
	const nav = document.querySelector('nav');
	const links = document.querySelectorAll('.link');
	const main = document.querySelector('main');
	const addButton = document.querySelector("#add-button");
	const moreButton = document.querySelector('#more-button');
	const form = document.querySelector("#form");
	const formTitle = document.querySelector("#form-title");
	const formId = document.querySelector("#form-id");
	const formType = document.querySelector('#form-type');
	const formInputs = document.querySelector("#form-inputs");
	const formSubmit = document.querySelector('#form-submit');
	const formEdit = document.querySelector('#form-edit');
	const formDelete = document.querySelector('#form-delete');
	const formCancel = document.querySelector('#form-cancel');
	const formClose = document.querySelector('#form-close');
	const overlay = document.querySelector('#overlay');
	const loading = document.querySelector('#loading');
	const settingsButton = document.querySelector('#settings-button');
	const search = document.querySelector('#search');
	const searchClose = document.querySelector('#search-close');
	const logout = document.querySelector('#logout-form');
	
	const filterMenu = document.querySelector('div.menu.filter');
	const filterOpener = filterMenu.querySelector('.opener');
	const filterSelector = filterMenu.querySelector('.selector');
	const filterButtons = filterMenu.querySelectorAll('.inner .filter');
	const filterPrev = filterMenu.querySelector('.selector .prev');
	const filterNext = filterMenu.querySelector('.selector .next');
	const filterCurrent = filterMenu.querySelector('.selector .current');
	
	form.addEventListener('submit', submitForm);
	formEdit.addEventListener('click', editForm);
	formDelete.addEventListener('click', deleteForm);
	formCancel.addEventListener('click', cancelForm);
	formClose.addEventListener('click', () => {showForm(false); });
	overlay.addEventListener('click', () => {showForm(false); });
	nav.addEventListener('mouseenter', () => {navHover(true); });
	nav.addEventListener('mouseleave', () => {navHover(false); });
	addButton.addEventListener('click', () => {
		openForm(new Transaction(settings, null));
	});
	settingsButton.addEventListener('click', () => openForm(settings));
	links.forEach(function(link) {
		link.addEventListener('click', showSection);
	});
	filterPrev.addEventListener('click', () => jumpFilter(-1));
	filterNext.addEventListener('click', () => jumpFilter(1));
	
	filterOpener.addEventListener('click', toggleMenu);
	filterOpener.addEventListener('blur', closeMenu);
	filterButtons.forEach(function(button) {
		button.addEventListener('click', filterData);
	});
	
	search.addEventListener('input', searchItems);
	searchClose.addEventListener('click', clearSearch);
	
	const intKeys = ['id', 'digits', 'day'];
	const curKeys = ['amount', 'balance', 'budget', 'limit'];
	const chkKeys = ['reconciled'];
	const fileKeys = ['import'];
	
	const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	
	var transactions = [];
	var accounts = [];
	var categories = [];
	var due = [];
	var settings;
	
	var totalAccounts = 0;
	var recentCount = 0;
	
	var searchTransactions = {};
	var searchAccounts = [];
	var searchCategories = [];
	
	var foundTransactions = [];
	var foundAccounts = [];
	var foundCategories = [];
	
	const today = new Date();
	
	var currentDate = today;
	
	var filter = 'all';
	var currentKey = 'all';
	var currentMonth = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1);
	filterCurrent.innerHTML = months[currentDate.getMonth()] + ' ' + currentDate.getFullYear();
	
	var setTimer = { timer: null };
	var searching = false;
	var menuCloseable = true;
	
	var sort = {transactions: 'date-desc', accounts: 'name-asc', categories: 'name-asc'}
	
	var offline = false;
	
	loading.classList.remove('hidden');
	
	fetch('server.php')
	.then(checkError)
	.then(data => parseData(data))
	.catch(err => console.error('!! ERROR READING JSON DATA', err));
	
	function checkError(response) {
		if(response.ok) {
			runOnline();
			return response.json();
		} else {
			console.error('!! OFFLINE. READING DATA FROM LOCAL STORAGE');
			runOffline();
			return JSON.parse(window.localStorage.getItem('data'));
		}
	}
	
	function runOffline() {
		offline = true;
		document.querySelector('#offline').classList.remove('hidden');
		document.querySelector('#form-buttons').classList.add('hidden');
		logout.classList.add('offline');
	}

	function runOnline() {
		offline = false;
		document.querySelector('#offline').classList.add('hidden');
		document.querySelector('#form-buttons').classList.remove('hidden');
		logout.classList.remove('offline');
	}
	
	function parseData(data) {
		
		transactions = [];
		accounts = [];
		categories = [];
		settings = new Setting(data.settings);
		
		data.transactions.forEach(function(transaction) {
			let newTransaction = new Transaction(settings, transaction);
			transactions.push(newTransaction);
			searchTransactions[newTransaction.id] = newTransaction.getText();
			
			balanceAccounts(newTransaction, 'from', -1);
			balanceAccounts(newTransaction, 'to', 1);
			
			balanceCategories(newTransaction);
			settings.transfer(newTransaction);
		});
		
		if(!offline) window.localStorage.setItem('data', JSON.stringify(data));
		
		printData();
	}
	
	function printData() {
		loading.classList.remove('hidden');
		
		setSortMenu();
		setFilterMenu();
		setDue();
		
		recentList.innerHTML = '';
		transactionList.innerHTML = '';
		accountList.innerHTML = '';
		categoryList.innerHTML = '';
		billList.innerHTML = '';
		budgetList.innerHTML = '';
		debtList.innerHTML = '';
		dueList.innerHTML = '';
		topCategoriesPie.innerHTML = '';
		topCategoriesList.innerHTML = '';
		
		recentCount = 0;
		moreButton.classList.add('hidden');
		totalAccounts = 0;
		
		transactions.sort(sortBy('date-desc'));
		transactions.forEach(printRecent);
		transactions.sort(sortBy(sort.transactions));
		transactions.forEach(printItem);
		
		accounts.sort(sortBy('day-asc'));
		accounts.forEach(printDebts);
		accounts.sort(sortBy(sort.accounts));
		accounts.forEach(printItem);
		
		categories.sort(sortBy('day-asc'));
		categories.forEach(printBills);
		categories.sort(sortBy('name-asc'));
		categories.forEach(printBudgets);
		categories.sort(sortBy(sort.categories));
		categories.forEach(printItem);
		
		due.sort(sortBy('name-asc'));
		due.sort(sortBy('day-asc'));
		due.forEach(printDue);
		
		totalExpenses.innerHTML = settings.printExpenses(currentKey);
		totalRevenue.innerHTML = settings.printRevenue(currentKey);
		totalNet.innerHTML = settings.printNet(currentKey);
		
		accounts.forEach(printPrimaries);
		totalAccount.innerHTML = settings.printAccounts(totalAccounts);
		
		printPie();
		
		printFilters();
		printIcons();
		
		loading.classList.add('hidden');
	}
	
	function printBills(item) {
		if(foundItem(item) && item.day > 0) {
			const li = document.createElement('li');
			
			li.className = item.type;
			li.innerHTML = item.printBill(currentMonth);
			li.addEventListener('click', () => {openForm(item); });
			
			billList.appendChild(li);
		}
	}
	
	function printBudgets(item) {
		if(foundItem(item) && item.day == 0 && item.budget > 0) {
			const li = document.createElement('li');
			
			li.className = item.type;
			li.innerHTML = item.printBudget(currentMonth);
			li.addEventListener('click', () => {openForm(item); });
			
			budgetList.appendChild(li);
		}
	}
	
	function printDebts(item) {
		if(foundItem(item) && item.day > 0) {
			const li = document.createElement('li');
			
			li.className = item.type;
			li.innerHTML = item.printDebt(currentMonth);
			li.addEventListener('click', () => {openForm(item); });
			
			debtList.appendChild(li);
		}
	}
	
	function printDue(item) {
		if(foundItem(item)) {
			const li = document.createElement('li');
			
			li.className = item.type;
			if(item.type == 'account') li.innerHTML = item.printDebt(currentMonth);
			else if(item.type == 'category') li.innerHTML = item.printBill(currentMonth);
			li.addEventListener('click', () => {openForm(item); });
			
			dueList.appendChild(li);
		}
	}
	
	function printRecent(item) {
		if(item.print(currentKey) && foundItem(item) && recentCount < 10) {
			const li = document.createElement('li');
			
			li.className = item.type;
			li.innerHTML = item.print(currentKey);
			
			li.dataset.id = item.id;
			li.addEventListener('click', () => {openForm(item); });
			recentList.appendChild(li);
			
			recentCount++;
		}
		if(recentCount >= 10) moreButton.classList.remove('hidden');
	}
	
	function printItem(item) {
		if(item.print(currentKey) && foundItem(item)) {
			const li = document.createElement('li');
			
			li.className = item.type;
			li.innerHTML = item.print(currentKey);
			
			switch(item.type) {
				case 'transaction':
					li.dataset.id = item.id;
					li.addEventListener('click', () => {openForm(item); });
					transactionList.appendChild(li);
					break;
				case 'account':
					li.addEventListener('click', () => {openForm(item); });
					accountList.appendChild(li);
					break;
				case 'category':
					li.addEventListener('click', () => {openForm(item); });
					categoryList.appendChild(li);
			}
		}
	}
	
	function printPrimaries(item) {
		if(item.name == settings.primary) {
			primaryAccount.innerHTML = item.print(currentKey);
			if(item.balances[currentKey]) totalAccounts += item.balances[currentKey];
		} else if(item.name == settings.secondary) {
			secondaryAccount.innerHTML = item.print(currentKey);
			if(item.balances[currentKey]) totalAccounts += item.balances[currentKey];
		}
	}
	
	function printPie() {
		let topCategories = [];
		let topCategoriesTotal = 0;
		let shownTotal = 0;
		categories.forEach(function(category) {
			if(category.isSlice(currentKey)) {
				topCategories.push({"name": category.name, "amount": category.getBalance(currentKey)});
				topCategoriesTotal += category.getBalance(currentKey);
			}
		});
		console.log('TOTAL', topCategoriesTotal);
		topCategories.sort(sortBy('amount-asc'));
		let count = 0;
		let prevPercent = 0;
		topCategories.every(function(category) {
			if(count < 4) {
				let slice = document.createElement('div');
				let row = document.createElement('li');
				let thisPercent = Math.floor(category.amount / topCategoriesTotal * 100);
				let endPercent = prevPercent + thisPercent;
				console.log('CATEGORY', category);
				slice.classList.add('slice');
				row.classList.add('slice-row');
				slice.style.background = 'conic-gradient(#0000 0% ' + prevPercent + '%, var(--hue' + (count + 1) + ') ' + prevPercent + '% ' + (endPercent + 1) + '%, #0000 ' + (endPercent + 1) + '%)';
				row.innerHTML = `<span class="hue hue${count + 1}">${thisPercent}&#37;</span><span class="name">${category.name}</span><span class="amount neg">${settings.money(category.amount)}</span>`;
				topCategoriesPie.appendChild(slice);
				topCategoriesList.appendChild(row);
				prevPercent = endPercent;
				count++;
				shownTotal += category.amount;
				return true;
			} else return false;
		});
		let lastSlice = document.createElement('div');
		let lastRow = document.createElement('li');
		lastSlice.classList.add('slice');
		lastRow.classList.add('slice-row');
		lastSlice.style.background = 'conic-gradient(#0000 0% ' + prevPercent + '%, var(--secondary) ' + prevPercent + '%)';
		lastRow.innerHTML = `<span class="hue secondary">${prevPercent}&#37;</span><span class="name">Other</span><span class="amount neg">${settings.money(topCategoriesTotal - shownTotal)}</span>`;
		topCategoriesPie.appendChild(lastSlice);
		topCategoriesList.appendChild(lastRow);
	}
	
	function printFilters() {
		document.querySelectorAll('section').forEach(function(section) {
			const span = section.querySelector('h2 span');
			let innerKey = parseKey(currentKey);
			let innerMonth = parseKey(currentMonth);
			if(span.dataset.key == 'key') span.innerHTML = innerKey;
			else if(span.dataset.key == 'month') span.innerHTML = innerMonth;
		});
	}
	
	function parseKey(key) {
		if(key == 'all') return 'All Time';
		else {
			if(key.includes('-')) return months[parseInt(key.split('-')[1]) - 1] + ' ' + key.split('-')[0];
			else return key;
		}
	}
	
	function balanceAccounts(item, key, factor) {
		let found = false;
		accounts.forEach(function(account) {
			if(account.name == item[key]) {
				found = true;
				account.transfer(item, factor);
			}
		});
		if(!found) {
			let newAccount = new Account(settings, item[key]);
			newAccount.transfer(item, factor);
			accounts.push(newAccount);
			searchAccounts.push(newAccount.getText());
		}
	}
	
	function balanceCategories(item) {
		let found = false;
		let factor = 0;
		if(item.flow == 'expense') factor = -1;
		else if(item.flow == 'revenue') factor = 1;
		categories.forEach(function(category) {
			if(category.name == item.category) {
				found = true;
				category.transfer(item, factor);
			}
		});
		if(!found) {
			let newCategory = new Category(settings, item.category);
			newCategory.transfer(item, factor);
			categories.push(newCategory);
			searchCategories.push(newCategory.getText());
		}
	}
	
	function setDue() {
		due = [];
		accounts.forEach(function(account) {
			if(account.isDue(currentMonth)) due.push(account);
		});
		categories.forEach(function(category) {
			if(category.isDue(currentMonth)) due.push(category);
		});
	}
	
	function openForm(item) {
		
		showForm(true);
		if(item.id) {
			formId.name = 'id';
			formId.value = item.id;
		} else {
			formId.name = 'name';
			formId.value = item.name;
		}
		
		formType.value = item.type;
		formInputs.innerHTML = item.form();
		
		switch(item.type) {
			case 'setting': settingsForm(); break;
			case 'transaction':
				if(item.id > 0) { cancelForm(); }
				else if(item.id == 0) { createForm(); }
				else {
					console.error('!! INVALID ID PARAMETER', item.id);
					return;
				} break;
			case 'account':
			case 'category': cancelForm(); break;
			default: console.error('!! INVALID FORM TYPE:', item.type);
		}
		
		formInputs.querySelectorAll('label.list').forEach(printFormList);
		
		printIcons();
	}
	
	function printFormList(label) {
		const input = label.querySelector('input');
		const list = label.querySelector('ul');
		const maxItems = 3;
		
		let arr;
		if(label.dataset.list == 'account') arr = accounts;
		if(label.dataset.list == 'category') arr = categories;
		
		list.innerHTML = '';
		
		arr.sort(sortBy('name-asc'));
		arr.sort(sortBy('count-desc'));
		
		arr.forEach(function(listItem) {
			let li = document.createElement('li');
			li.innerHTML = listItem.name;
			li.className = 'hidden';
			li.addEventListener('click', function(e) {
				input.value = e.target.innerHTML;
			});
			list.appendChild(li);
		});
		
		input.addEventListener('input', function(e) {
			let counter = 0;
			list.classList.add('hidden');
			label.querySelectorAll('ul li').forEach(function(listItem) {
				listItem.classList.add('hidden');
				if(listItem.innerHTML.toLowerCase().includes(e.target.value.toLowerCase()) && counter < maxItems) {
					listItem.classList.remove('hidden');
					list.classList.remove('hidden');
					counter++;
				}
			});
		});
		
		input.addEventListener('focus', function(e) {
			list.classList.add('hidden');
			if(!e.relatedTarget || !e.relatedTarget.classList.contains('datalist')) {
				let counter = 0;
				label.querySelectorAll('ul li').forEach(function(listItem) {
					listItem.classList.add('hidden');
					if(listItem.innerHTML.toLowerCase().includes(e.target.value.toLowerCase()) && counter < maxItems) {
						listItem.classList.remove('hidden');
						list.classList.remove('hidden');
						counter++;
					}
				});
			}
		});
		
		input.addEventListener('blur', function(e) {
			if(!e.relatedTarget || !e.relatedTarget.classList.contains('datalist')) {
				list.classList.add('hidden');
				formInputs.querySelectorAll('label.list').forEach(function(label) {
					label.querySelectorAll('ul li').forEach(function(element) {
						element.classList.add('hidden');
					});
				});
			}
		});
	}
	
	function printMenus() {
		const sortMenus = document.querySelectorAll('div.menu.sort');
		sortMenus.forEach(function(menu) {
			let openButton = document.createElement('button');
			let sortCheck = document.createElement('span');
			sortCheck.classList.add('icon', 'hidden');
			sortCheck.dataset.icon = 'submit';
			openButton.classList.add('icon', 'opener');
			openButton.type = 'button';
			openButton.dataset.icon = 'sort';
			openButton.addEventListener('click', toggleMenu);
			openButton.addEventListener('blur', closeMenu);
			menu.appendChild(openButton);
			let innerDiv = document.createElement('div');
			innerDiv.classList.add('inner', 'hidden');
			let title = document.createElement('h3');
			title.innerHTML = 'Sort By';
			innerDiv.appendChild(title);
			menu.dataset.sort.split(',').forEach(function(key) {
				let sortButton = document.createElement('button');
				sortButton.classList.add('sort');
				sortButton.type = 'button';
				sortButton.dataset.sort = key;
				sortButton.dataset.type = menu.parentNode.parentNode.id;
				sortButton.innerHTML = cap(key);
				sortButton.appendChild(sortCheck.cloneNode(true));
				sortButton.addEventListener('click', sortData);
				innerDiv.appendChild(sortButton);
			});
			innerDiv.appendChild(document.createElement('hr'));
			let ascButton = document.createElement('button');
			let descButton = document.createElement('button');
			ascButton.classList.add('sort');
			descButton.classList.add('sort');
			ascButton.type = 'button';
			descButton.type = 'button';
			ascButton.dataset.sort = 'asc';
			descButton.dataset.sort = 'desc';
			ascButton.dataset.type = menu.parentNode.parentNode.id;
			descButton.dataset.type = menu.parentNode.parentNode.id;
			ascButton.innerHTML = 'Ascending';
			descButton.innerHTML = 'Descending';
			ascButton.addEventListener('click', orderData);
			descButton.addEventListener('click', orderData);
			ascButton.appendChild(sortCheck.cloneNode(true));
			descButton.appendChild(sortCheck);
			innerDiv.appendChild(ascButton);
			innerDiv.appendChild(descButton);
			menu.appendChild(innerDiv);
		});
	}
	
	function parseDate(d) {
		let newD = new Date(d);
		return new Date(newD.getTime()).toISOString().split('T')[0];
	}
	
	function validTransaction(t) {
		if(t['date'] == '') return 'INVALID DATE ' + t['date'];
		if(t['from'] == '') return 'INVALID FROM ' + t['from'];
		if(t['to'] == '') return 'INVALID TO ' + t['to'];
		if(t['amount'] <= 0) return 'INVALID AMOUNT ' + t['amount'];
		if(t['category'] == '') return 'INVALID CATEGORY ' + t['category'];
		if(t['reconciled'] < 0 || t['reconciled'] > 1) return 'INVALID RECONCILED ' + t['reconciled'];
		return 'VALID';
	}
	
	async function parseCSV(file) {
		return await new Promise((resolve, reject) => {
			let extension = file.name.substring(file.name.lastIndexOf('.')).toUpperCase();
			if(extension !== '.CSV') {
				console.error('!! WRONG FILE TYPE. PLEASE UPLOAD A VALID CSV FILE', file.name);
				reject();
			}
			
			try {
				let reader = new FileReader();
				reader.readAsText(file);
				reader.onload = function(e) {
					let jsonData = [];
					let headers = [];
					let rows = e.target.result.split(/\r\n|\r|\n/);
					for(let i = 0; i < rows.length; i++) {
						let cells = rows[i].split(',');
						let rowData = {};
						for(let j = 0; j < cells.length; j++) {
							if(i == 0) headers.push(cells[j].trim());
							else if(cells[j].trim() != '') {
								switch(headers[j]) {
									case 'date': rowData[headers[j]] = parseDate(cells[j].trim()); break;
									case 'from':
									case 'to':
									case 'category': rowData[headers[j]] = cells[j].trim().replaceAll('"', ''); break;
									case 'amount': rowData[headers[j]] = parseInt(Math.round(parseFloat(cells[j].trim()) * 100)); break;
									case 'reconciled': rowData[headers[j]] = parseInt(cells[j].trim()); break;
									default:
										console.error('!! HEADERS INCORRECT. PLEASE UPLOAD A VALID CSV FILE', headers[j]);
										reject();
								}
							}
						}
						if(i != 0 && Object.keys(rowData).length != 0) {
							if(validTransaction(rowData) == 'VALID') jsonData.push(rowData);
							else {
								console.error('!! INVALID DATA. PLEASE UPLOAD A VALID CSV FILE', validTransaction(rowData), rowData)
								reject();
							}
						}
					}
					resolve(jsonData);
				}
			} catch(err) {
				reject(err);
			}
		});
	}
	
	function submitForm(event) {
		event.preventDefault();
		showForm(false);
		loading.classList.remove('hidden');
		
		let ready = true;
		const inputs = event.target.querySelectorAll('input');
		let newItem = {};
		
		let check = function() {
			if(ready === true) {
				return fetch('server.php', {
					method: "POST",
					headers: {
						'Content-Type': 'application/json',
						'Accept': 'application/json'
					},
					body: JSON.stringify(newItem)
				})
				.then(checkError)
				.then(data => parseData(data))
				.catch(err => console.error('>> ERROR READING JSON DATA', err));
			}
			setTimeout(check, 1000);
		}
		
		inputs.forEach(function(input) {
			if(intKeys.indexOf(input.name) >= 0) newItem[input.name] = parseInt(input.value);
			else if(curKeys.indexOf(input.name) >= 0) newItem[input.name] = parseInt(Math.round(parseFloat(input.value) * 100));
			else if(chkKeys.indexOf(input.name) >= 0) input.checked ? newItem[input.name] = 1 : newItem[input.name] = 0;
			else if(fileKeys.indexOf(input.name) >= 0 && input.files.length > 0) {
				ready = false;
				parseCSV(input.files[0]).then(data => {
					ready = true;
					newItem[input.name] = data;
				}).catch(err => console.error(err));
			}
			else newItem[input.name] = input.value;
		});
		
		check();
	}
	
	function createForm() {
		formTitle.innerHTML = 'Create ' + cap(formType.value);
		lockForm(false);
		toggleFormButtons(true, false, false, false);
	}
	
	function editForm() {
		lockForm(false);
		
		if(formId.name == 'id') {
			formTitle.innerHTML = 'Edit ' + cap(formType.value);
			toggleFormButtons(true, false, true, true);
		} else {
			formTitle.innerHTML = 'Edit ' + cap(formId.value);
			toggleFormButtons(true, false, false, true);
		}
	}
	
	function deleteForm() {
		lockForm(true);
		
		formTitle.innerHTML = 'Delete ' + cap(formType.value) + '?';
		if(formId.name == 'id') formId.value = parseInt(formId.value) * -1;
		
		toggleFormButtons(true, false, false, true);
	}
	
	function cancelForm() {
		lockForm(true);
		
		if(formId.name == 'id') {
			formId.value = Math.abs(parseInt(formId.value));
			formTitle.innerHTML = cap(formType.value);
		} else {
			formTitle.innerHTML = cap(formId.value);
		}
		toggleFormButtons(false, true, false, false);
	}
	
	function settingsForm() {
		lockForm(false);
		
		formTitle.innerHTML = 'Settings';
		toggleFormButtons(true, false, false, false);
		
		logout.classList.remove('hidden');
		if(!offline) {
			form.querySelector('#export').addEventListener('click', exportData);
			form.querySelector('#export').classList.remove('hidden');
		} else form.querySelector('#export').classList.add('hidden');
		
		form.querySelector('#import').addEventListener('click', importData);
	}
	
	function exportData(event) {
		showForm(false);
	}
	
	function importData(event) {
		const label = form.querySelector('#import-label');
		
		if(label.classList.contains('hidden')) {
			label.classList.remove('hidden');
			form.querySelector('#import').innerHTML = 'Cancel';
			form.querySelector('#import').classList.add('neg');
		} else {
			label.classList.add('hidden');
			form.querySelector('#import').innerHTML = 'Import Transactions from CSV';
			form.querySelector('#import').classList.remove('neg');
		}
	}
	
	const CSVToJSON = csv => {
        const lines = csv.split('\n');
        const keys = lines[0].split(',');
        return lines.slice(1).map(line => {
            return line.split(',').reduce((acc, cur, i) => {
                const toAdd = {};
                toAdd[keys[i]] = cur;
                return { ...acc, ...toAdd };
            }, {});
        });
    };
	
	function showForm(show) {
		if(show) {
			form.classList.remove('hidden');
			overlay.classList.remove('hidden');
		} else {
			form.classList.add('hidden');
			overlay.classList.add('hidden');
			logout.classList.add('hidden');
		}
	}
	
	function toggleFormButtons(sub, edi, del, can) {
		sub ? formSubmit.classList.remove('hidden') : formSubmit.classList.add('hidden');
		edi ? formEdit.classList.remove('hidden') : formEdit.classList.add('hidden');
		del ? formDelete.classList.remove('hidden') : formDelete.classList.add('hidden');
		can ? formCancel.classList.remove('hidden') : formCancel.classList.add('hidden');
	}
	
	function lockForm(lock) {
		const allFormInputs = formInputs.querySelectorAll('input');
		allFormInputs.forEach(function(input) { input.disabled = lock; });
	}
	
	function navHover(hovering) {
		hovering ? main.classList.add('hover') : main.classList.remove('hover');
	}
	
	function cap(str) {
		return str.charAt(0).toUpperCase() + str.slice(1);
	}
	
	function showSection(event) {
		const sections = document.querySelectorAll('section');
		
		sections.forEach(function(section) {
			section.classList.add('hidden');
		});
		
		links.forEach(function(link) {
			link.classList.remove('selected');
		});
		
		switch(event.target.dataset.link) {
			case 'home':
				document.querySelector('#recent').classList.remove('hidden');
				document.querySelector('#flow').classList.remove('hidden');
				document.querySelector('#personal').classList.remove('hidden');
				document.querySelector('#due').classList.remove('hidden');
				document.querySelector('#budgets').classList.remove('hidden');
				break;
			case 'transaction':
				document.querySelector('#flow').classList.remove('hidden');
				document.querySelector('#transactions').classList.remove('hidden');
				break;
			case 'account':
				document.querySelector('#personal').classList.remove('hidden');
				document.querySelector('#debts').classList.remove('hidden');
				document.querySelector('#accounts').classList.remove('hidden');
				break;
			case 'category':
				document.querySelector('#bills').classList.remove('hidden');
				document.querySelector('#budgets').classList.remove('hidden');
				document.querySelector('#categories').classList.remove('hidden');
				break;
			case 'data':
				document.querySelector('#top-categories').classList.remove('hidden');
				document.querySelector('#top-categories-pie').classList.remove('hidden');
				break;
		}
		document.querySelector('#' + event.target.dataset.link + '-button').classList.add('selected');
		window.scrollTo(0, 0);
	}
	
	function filterData(event) {
		filter = event.target.dataset.filter;
		menuCloseable = false;
		
		jumpFilter(0);
	}
	
	function jumpFilter(jump) {
		menuCloseable = false;
		switch(filter) {
			case 'monthly':
				currentDate.setMonth(currentDate.getMonth() + jump);
				currentKey = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1);
				currentMonth = currentKey;
				break;
			case 'yearly':
				currentDate.setFullYear(currentDate.getFullYear() + jump);
				currentKey = currentDate.getFullYear() + '';
				currentMonth = currentKey + '-' + (currentDate.getMonth() + 1);
				break;
			case 'all':
				currentDate.setMonth(currentDate.getMonth() + jump);
				currentMonth = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1);
				currentKey = 'all';
				break;
		}
		
		filterCurrent.innerHTML = months[currentDate.getMonth()] + ' ' + currentDate.getFullYear();
		
		printData();
	}
	
	function searchItems(event) {
		let searchValue = event.target.value.toLowerCase();
		foundTransactions = [];
		foundAccounts = [];
		foundCategories = [];
		searching = false;
		
		if(searchValue.length < 1) searchClose.classList.add('hidden');
		else searchClose.classList.remove('hidden');
		
		if(searchValue.length > 1) {
			searching = true;
			for(const id in searchTransactions) {
				if(searchTransactions[id].includes(searchValue)) foundTransactions.push(parseInt(id));
			}
			searchAccounts.forEach(function(account) {
				if(account.includes(searchValue)) foundAccounts.push(account);
			});
			searchCategories.forEach(function(category) {
				if(category.includes(searchValue)) foundCategories.push(category);
			});
		}
		
		printData();
	}
	
	function clearSearch(event) {
		search.value = '';
		search.blur();
		event.target.classList.add('hidden');
		
		foundTransactions = [];
		foundAccounts = [];
		foundCategories = [];
		searching = false;
		
		printData();
	}
	
	function foundItem(i) {
		if(!searching) return true;
		else {
			switch(i.type) {
				case 'transaction': if(foundTransactions.includes(i.id)) return true; break;
				case 'account': if(foundAccounts.includes(i.name.toLowerCase())) return true; break;
				case 'category': if(foundCategories.includes(i.name.toLowerCase())) return true; break;
			}
			if(i.name == settings.primary || i.name == settings.secondary) return true;
			else return false;
		}
	}
	
	function toggleMenu(event) {
		event.target.focus();
		let innerMenu = event.target.parentNode.querySelector('.inner');
		if(innerMenu.classList.contains('hidden')) innerMenu.classList.remove('hidden');
		else innerMenu.classList.add('hidden');
	}
	
	function closeMenu(event) {
		let innerMenu = event.target.parentNode.querySelector('.inner');
		setTimer.timer = setTimeout(function () {
			if(menuCloseable) innerMenu.classList.add('hidden');
			else {
				menuCloseable = true;
				event.target.focus();
			}
		}, 100);
	}
	
	function sortData(event) {
		let type = event.target.dataset.type;
		let key = event.target.dataset.sort;
		sort[type] = key + '-' + sort[type].split('-')[1];
		
		printData();
	}
	
	function orderData(event) {
		let type = event.target.dataset.type;
		let order = event.target.dataset.sort;
		sort[type] = sort[type].split('-')[0] + '-' + order;
		
		printData();
	}
	
	function sortBy(type) {
		let key = type.split('-')[0];
		let order = type.split('-')[1];
		let flip = 1;
		if(order == 'desc') flip = -1;
		return (a, b) => a[key] == b[key] ? b.id - a.id : a[key] > b[key] ? flip : flip * -1;
	}
	
	function setSortMenu() {
		let sortMenus = document.querySelectorAll('div.menu.sort');
		sortMenus.forEach(function(menu) {
			let buttons = menu.querySelectorAll('.inner button');
			let type = menu.parentNode.parentNode.id;
			let key = sort[type].split('-')[0];
			let order = sort[type].split('-')[1];
			buttons.forEach(function(button) {
				let check = button.querySelector('span');
				check.classList.add('hidden');
				if(key == button.dataset.sort || order == button.dataset.sort)
					check.classList.remove('hidden');
			});
		});
	}
	
	function setFilterMenu() {
		filterButtons.forEach(function(button) {
			let check = button.querySelector('span');
			check.classList.add('hidden');
			if(button.dataset.filter == filter) check.classList.remove('hidden');
		});
	}
});
