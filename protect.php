<?php

session_start();
	
if(isset($_POST['logout'])) {
	session_destroy();
	unset($_SESSION);
	$past = time() - 3600 * 24 * 30;
	setcookie('remember', '', $past);
}

if(!isset($_SESSION['username'])) {
	header('Location: login.php');
	exit();
}

?>