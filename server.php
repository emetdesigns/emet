<?php

if(!file_exists('data.php')) createData('data.php');
require 'data.php';

if(file_get_contents('php://input')) {
	$settings = unserialize($raw_settings);
	$transactions = unserialize($raw_transactions);
	
	$newItem = json_decode(file_get_contents('php://input'), true);
	
	switch($newItem['type']) {
		case 'transaction':
			if($newItem['id'] > 0) { // UPDATE
				$itemId = findId($transactions, $newItem['id']);
				if($itemId >= 0) $transactions[$itemId] = $newItem;
			} else if($newItem['id'] < 0) { // DELETE
				array_splice($transactions, findId($transactions, abs($newItem['id'])), 1);
			} else if($newItem['id'] == 0) { // CREATE
				$newItem['id'] = nextId($transactions);
				$transactions[] = $newItem;
			}
			break;
		case 'setting':
			if(isset($newItem['import']) && gettype($newItem['import']) == 'array') {
				$transactions = importData($newItem['import']);
			}
			unset($newItem['import']);
			$oldhash = $settings['password'];
			$settings = array_merge($settings, $newItem);
			$settings['password'] = $oldhash;
			if($newItem['password'] !== '') $settings['password'] = password_hash($newItem['password'], PASSWORD_DEFAULT);
			$settings = clearOldSettings($settings, $transactions);
			break;
		case 'account':
			if($newItem['newname'] != $newItem['name']) {
				$transactions = changeNames($transactions, 'from', $newItem['name'], $newItem['newname']);
				$transactions = changeNames($transactions, 'to', $newItem['name'], $newItem['newname']);
				unset($settings['accounts'][$newItem['name']]);
				$newItem['name'] = $newItem['newname'];
			}
			$settings['accounts'][$newItem['name']]['day'] = $newItem['day'];
			$settings['accounts'][$newItem['name']]['budget'] = $newItem['budget'];
			$settings['accounts'][$newItem['name']]['limit'] = $newItem['limit'];
			$settings = clearOldSettings($settings, $transactions);
			break;
		case 'category':
			if($newItem['newname'] != $newItem['name']) {
				$transactions = changeNames($transactions, 'category', $newItem['name'], $newItem['newname']);
				unset($settings['categories'][$newItem['name']]);
				$newItem['name'] = $newItem['newname'];
			}
			$settings['categories'][$newItem['name']]['day'] = $newItem['day'];
			$settings['categories'][$newItem['name']]['budget'] = $newItem['budget'];
			$settings = clearOldSettings($settings, $transactions);
			break;
	}
	
	writeData($settings, $transactions, 'data.php');
	
	$data['settings'] = $settings;
	$data['transactions'] = $transactions;
	
	echo json_encode($data);
} else if($_SERVER['HTTP_SEC_FETCH_MODE'] == 'cors') {
	$settings = unserialize($raw_settings);
	$transactions = unserialize($raw_transactions);
	
	$data['settings'] = $settings;
	$data['transactions'] = $transactions;
	
	echo json_encode($data);
} else {
	echo '';
}

function nextId($arr) {
	$nextId = 1;
	foreach($arr as $obj) {
		if($nextId <= $obj['id']) $nextId = $obj['id'] + 1;
	}
	return $nextId;
}

function findId($arr, $id) {
	$i = 0;
	foreach($arr as $obj) {
		if($obj['id'] == $id) return $i;
		$i++;
	}
	return -1;
}

function debug($msg) {
	error_log(print_r($msg, TRUE));
}

function writeData($arr_s, $arr_t, $file) {
	$raw_s = '$raw_settings=' . "'" . serialize($arr_s) . "'" . ';';
	$raw_t = '$raw_transactions=' . "'" . serialize($arr_t) . "'" . ';';
	file_put_contents('data.php', '<?php ' . $raw_s . $raw_t . ' ?>');
}

function createData($file_name) {
	$new_s = [];
	$new_s['type'] = 'setting';
	$new_s['id'] = 1;
	$new_s['username'] = 'admin';
	$new_s['password'] = password_hash('admin', PASSWORD_DEFAULT);
	$new_s['primary'] = '';
	$new_s['secondary'] = '';
	$new_s['locale'] = 'en-US';
	$new_s['currency'] = 'USD';
	$new_s['digits'] = 2;
	$new_s['format'] = 'accounting';
	$new_s['categories'] = [];
	$new_s['accounts'] = [];
	$new_t = [];
	writeData($new_s, $new_t, $file_name);
}

function changeNames($arr, $key, $old, $new) {
	$newArr = [];
	foreach($arr as $obj) {
		$newObj = $obj;
		if($obj[$key] == $old) $newObj[$key] = $new;
		$newArr[] = $newObj;
	}
	return $newArr;
}

function importData($arr) {
	$newArr = [];
	$id = 1;
	foreach($arr as $obj) {
		$newObj = [];
		$newObj['type'] = 'transaction';
		$newObj['id'] = $id;
		$newObj['date'] = $obj['date'];
		$newObj['from'] = $obj['from'];
		$newObj['to'] = $obj['to'];
		$newObj['amount'] = $obj['amount'];
		$newObj['category'] = $obj['category'];
		$newObj['reconciled'] = $obj['reconciled'];
		$newArr[] = $newObj;
		$id++;
	}
	return $newArr;
}

function clearOldSettings($item, $trans) {
	$accountList = [];
	$categoryList = [];
	if(isset($item['import'])) unset($item['import']);
	if(isset($item['accounts']) || isset($item['categories'])) {
		foreach($trans as $transaction) {
			if(!in_array($transaction['from'], $accountList)) $accountList[] = $transaction['from'];
			if(!in_array($transaction['to'], $accountList)) $accountList[] = $transaction['to'];
			if(!in_array($transaction['category'], $categoryList)) $categoryList[] = $transaction['category'];
		}
	}
	if(isset($item['accounts'])) {
		$newAccounts = [];
		foreach($item['accounts'] as $name => $account) {
			if($account['day'] > 0 && in_array($name, $accountList, true)) $newAccounts[$name] = $account;
		}
		$item['accounts'] = $newAccounts;
	}
	if(isset($item['categories'])) {
		$newCategories = [];
		foreach($item['categories'] as $name => $category) {
			if(($category['day'] > 0 || $category['budget'] > 0) && in_array($name, $categoryList, true)) $newCategories[$name] = $category;
		}
		$item['categories'] = $newCategories;
	}
	return $item;
}

?>