// Service Worker

var cacheName = 'emet-v1.7.0';
var cacheFiles = [
	'/',
	'/classes.js',
	'/favicon.ico',
	'/icons.js',
	'/index.php',
	'/main.js',
	'/styles.css',
	'/fonts/ubuntu-v20-latin-300.woff',
	'/fonts/ubuntu-v20-latin-300.woff2',
	'/fonts/ubuntu-v20-latin-700.woff',
	'/fonts/ubuntu-v20-latin-700.woff2',
	'/fonts/ubuntu-v20-latin-regular.woff',
	'/fonts/ubuntu-v20-latin-regular.woff2'
];

self.addEventListener('install', function(e) {
	e.waitUntil(
		caches.open(cacheName).then(function(cache) {
			return cache.addAll(cacheFiles);
		})
	);
});

self.addEventListener('fetch', function(e) {
	e.respondWith(
		caches.match(e.request).then(function(response) {
			return response ? response : fetch(e.request).catch(function(e) {
				var init = { "status" : 500, "statusText" : "offline" };
				return new Response("", init);
			});
		})
	);
});